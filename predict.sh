export OUTPUT_FILE=allennlp_tests/predictions/predictions.json

allennlp evaluate \
  --output-file $OUTPUT_FILE \
  --include-package allennlp_tests \
  allennlp_tests/modelv26/ \
  allennlp_tests/datasets/tta_input.raw#allennlp_tests/datasets/tta_sent_1.npy
