# Here is where the magic happens
# Honestly I just wanna run a simple fucking lstm at the moment
# just get me 2 embedding selectors which pump input into an lstm
# and empty out a prediction for a word which gives our cross entropy loss
# and perplexity
# note: in the allennlp.modules.token_embedders.embedding we have an option to load
# pretrained embeddings so we'll basically use our fasttext.cc vectors here
# note that while I have downloaded the vectors, I'm not running them
# it's simply because the size of the vectors is too big
# eng vectors have size 5.3G
# hin vectors have size 342 MB
# we'll need to cite the papers too

# let's run a simple config model using our fasttext eng vectors
# that's help us in doing stuff

# it is fucking time to move on
# we just executed multiple runs on a different sample dataset, with the config originating from
# sample json files in allennlp
# so what do we do?
# try running something for fucks sake lol
{
    "dataset_reader": {
      "type": "semeval-reader",
      "token_indexers": {
        "tokens": {
          "type": "single_id"
        },
        "token_characters": {
          "type": "characters"
        }
        # OLD we'd like to put token characters in too, but a little later
        # especially if we only want to train on romanized code mixed tweets from the dataset
      }
    },
    "train_data_path" : "",
    "model": {
        "type": "language_model",
        "bidirectional": false,
        "num_samples": 8192,
        "token_embedders": {
          # here we want our fasttext vectors based on the labels hi or en
          # what if I concatenate the output from both? and choose the half which I want later?

          # looks like we're using simple cnn based token embeddings for the moment
          # we can definitely later change it however we want too
          # this outputs a 50 dim embedding for the moment
          "token_characters": {
            "type": "character_encoding",
            "embedding": {
              "embedding_dim": 8
            },
            "encoder": {
              "type": "cnn",
              "embedding_dim": 8,
              "num_filters": 50,
              "ngram_filter_sizes": [
                  5
              ]
            },
            "dropout": 0.2
          }
        },
        "dropout": 0.1,
        "contextualizer": {
          # here is what we want our inner layers to be, this is basically a seq2seqEncoder
          # let's try something atleast, use the basic bidirectional language model as an inspiration
          # from the allennlp-models repository
          "type": "bidirectional_language_model_transformer",
          "input_dim": 50,
          "hidden_dim": 200,
          "num_layers": 6,
          "dropout": 0.1,
          "input_dropout": 0.1
        },
    },
    "data_loader": {
      "type": "default",
      "batch_size": 32
    },
    "trainer": {
      "num_epochs": 10,
      "optimizer": {
        "type": "dense_sparse_adam" # inspired from a training config in allennlp-models for lm
      },
      "learning_rate_scheduler": { # will have to see what this does obviously first
        "type": "noam"
      },
    }
}