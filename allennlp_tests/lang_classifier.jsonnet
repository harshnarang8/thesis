{
    "dataset_reader": {
        "type": "semeval-reader-2",
        "token_indexers": {
            "hin": {
                "type": "single_id"
            },
            "eng": {
                "type": "single_id"
            },
            "token_characters": {
                "type": "characters"
            }
        }
    },
    "train_data_path": "allennlp_tests/datasets/tsd_processed_split_train.txt",
    "validation_data_path": "allennlp_tests/datasets/tsd_processed_split_val.txt",
    "model": {
        "type": "hinglish-classifier",
        "text_field_embedder": {
            "type": "hinglish-embedder",
            "token_embedders": {
                "hin": {
                    "type": "embedding",
                    "embedding_dim": 300,
                    "projection_dim": 15,
                    "pretrained_file": "allennlp_tests/wiki.hi.align.vec",
                    "trainable": false
                },
                "eng": {
                    "type": "embedding",
                    "embedding_dim": 300,
                    "projection_dim": 15,
                    "pretrained_file": "allennlp_tests/wiki.en.align.vec",
                    "trainable": false
                },
                "token_characters": {
                    "type": "character_encoding",
                    "embedding": {
                        "embedding_dim": 8,
                        "vocab_namespace": "token_characters"
                    },
                    "encoder": {
                        "type": "cnn",
                        "embedding_dim": 8,
                        "num_filters": 50,
                        "ngram_filter_sizes": [
                            5
                        ]
                    },

                    "dropout": 0.2
                }
            },
        },
        "dropout": 0.5,
        // "seq2vec_encoder": {
        //     "type": "cnn",
        //     "num_filters": 10,
        //     "embedding_dim": 65,
        //     "output_dim": 20
        // },
        "seq2vec_encoder": {
            "type": "augmented_lstm",
            "input_size": 65,
            "hidden_size": 50,
            "recurrent_dropout_probability": 0.5
        },
    },
    "data_loader": {
        "type": "default",
        "batch_size": 32
    },
    "trainer": {
        "optimizer": "adam",
        "num_epochs": 60,
        "patience": 10,
        "cuda_device": -1
    }
}