import itertools

def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False

def divide_tweets(myfile):
    with open(myfile, 'r') as read_file:
        counter = 0
        wrfileTra = open('../datasets/tsd_processed_split_train.txt','w')
        wrfileVal = open('../datasets/tsd_processed_split_val.txt','w')
        wrfile = wrfileTra
        for is_divider, lines in itertools.groupby(read_file, _is_divider):
                # Ignore the divider chunks, so that `lines` corresponds to the words
                # of a single sentence.
                if not is_divider:
                    counter += 1
                    if counter >= 10000:
                        wrfile = wrfileVal
                    for line in lines:
                        wrfile.write(line)
                    wrfile.write('\n')
                

        print(counter)

# divide_tweets('../datasets/tsd_processed_all_2.txt')