{
    "dataset_reader": {
        "type": "semeval-reader-2",
        "token_indexers": {
            "tokens": {
                "type": "single_id"
            },
            "token_characters": {
                "type": "characters"
            }
        }
    },
    "train_data_path": "allennlp_tests/datasets/tsd_processed_split_train.txt",
    "validation_data_path": "allennlp_tests/datasets/tsd_processed_split_val.txt",
    "model": {
        "type": "base-classifier",
        "text_field_embedder": {
            "token_embedders": {
                "tokens": {
                    "type": "embedding",
                    "embedding_dim": 300,
                },
                "token_characters": {
                    "type": "character_encoding",
                    "embedding": {
                        "embedding_dim": 8,
                        "vocab_namespace": "token_characters"
                    },
                    "encoder": {
                        "type": "cnn",
                        "embedding_dim": 8,
                        "num_filters": 50,
                        "ngram_filter_sizes": [
                            5
                        ]
                    },

                    "dropout": 0.2
                }
            },
        },
        "dropout": 0.5,
        // "seq2vec_encoder": {
        //     "type": "cnn",
        //     "num_filters": 10,
        //     "embedding_dim": 65,
        //     "output_dim": 20
        // },
        "seq2vec_encoder": {
            "type": "augmented_lstm",
            "input_size": 65,
            "hidden_size": 50,
            "recurrent_dropout_probability": 0.5
        },
    },
    "data_loader": {
        "type": "default",
        "batch_size": 32
    },
    "trainer": {
        "optimizer": "adam",
        "num_epochs": 60,
        "patience": 10,
        "cuda_device": -1
    }
}