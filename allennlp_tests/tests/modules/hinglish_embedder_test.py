# experimenting with values, code is inspired from the test cases from basicTextFieldEmbedder
# from the allennlp github repository
# https://github.com/allenai/allennlp/blob/master/tests/modules/text_field_embedders/basic_text_field_embedder_test.py

import pytest
import torch

from allennlp.common import Params
from allennlp.common.checks import ConfigurationError
from allennlp.data import Vocabulary
from allennlp.modules.text_field_embedders import BasicTextFieldEmbedder
from allennlp_tests.modules.hinglish_embedder import HinglishEmbedder

from allennlp.common.testing import AllenNlpTestCase


class TestHinglishEmbedder(AllenNlpTestCase):
    def setup_method(self):
        super().setup_method()
        self.vocab = Vocabulary()
        print(self.vocab.add_token_to_namespace("1"))
        print(self.vocab.add_token_to_namespace("2"))
        print(self.vocab.add_token_to_namespace("3"))
        print(self.vocab.add_token_to_namespace("4"))
        print(self.vocab.add_token_to_namespace("5"))
        print(self.vocab.get_token_to_index_vocabulary())
        params = Params(
            {
                "token_embedders": {
                    "hin": {
                        "type": "embedding",
                        "embedding_dim": 3,
                        "num_embeddings": 20
                        },
                    "eng": {
                        "type": "embedding",
                        "embedding_dim": 3,
                        "num_embeddings": 20
                        },
                    "characters": {
                        "type": "character_encoding",
                        "embedding": {
                        "embedding_dim": 8,
                        "num_embeddings": 15,
                        "vocab_namespace": "token_characters"
                        },
                        "encoder": {
                            "type": "cnn",
                            "embedding_dim": 8,
                            "ngram_filter_sizes": [
                                3
                            ],
                            "num_filters": 4
                        }
                    }
                }
            }
        )
        self.token_embedder = HinglishEmbedder.from_params(vocab=self.vocab, params=params)
        print(self.vocab.get_token_to_index_vocabulary())
        self.inputs = {
            "hin": {"tokens": torch.LongTensor([[0, 2, 3, 5, 1, 6]])},
            "eng": {"tokens": torch.LongTensor([[1, 4, 3, 2, 1, 12]])},
            "characters": {"token_characters": (torch.rand(1,6,7)*15).long()}
        }

    def test_out_dim(self):
        assert self.token_embedder.get_output_dim() == 3
    
    def test_forward(self):
        mask = torch.BoolTensor([[0, 0, 0, 1, 1, 1]])
        print(self.token_embedder(self.inputs, mask))
        print(self.token_embedder(self.inputs, mask).size()) 