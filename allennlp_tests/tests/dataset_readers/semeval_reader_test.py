from allennlp.common.testing import AllenNlpTestCase
from allennlp.common.util import ensure_list

from allennlp_tests.dataset_readers import SemevalReader, SemevalReader2


class TestSemevalReader(AllenNlpTestCase):
    def test_read_from_file(self):
        reader = SemevalReader()

        instances = ensure_list(reader.read('allennlp_tests/datasets/nice_tweets.txt'))
        
        assert len(instances) == 6632
        print(instances[1])

        # print("YOLO")

    def test2(self):
        reader = SemevalReader()
        instances = ensure_list(reader.read('allennlp_tests/tests/fixtures/semeval_tests.txt'))
        assert len(instances) == 2
        print(instances[1])

    def test3(self):
        reader = SemevalReader()

        instances = ensure_list(reader.read('allennlp_tests/datasets/nice_tweets_train.txt'))
        
        assert len(instances) == 5000
        print(instances[1])

    def test4(self):
        reader = SemevalReader()

        instances = ensure_list(reader.read('allennlp_tests/datasets/nice_tweets_val.txt'))
        
        assert len(instances) == 1632
        print(instances[1])
    
    def test5(self):
        # this test is being written for separation of full stops, replacing urls, etc
        reader = SemevalReader2()
        instances = ensure_list(reader.read('allennlp_tests/tests/fixtures/semeval_tests.txt'))

        print(instances[0])
        print(instances[1])