from allennlp.common.testing import AllenNlpTestCase
from allennlp.common.util import ensure_list

from allennlp_tests.dataset_readers import LaserReader


class TestLaserReader(AllenNlpTestCase):
    def test_read_from_file(self):
        reader = LaserReader()

        instances = ensure_list(reader.read('allennlp_tests/datasets/train_split.raw#allennlp_tests/datasets/sentiments_train.npy'))
        
        assert len(instances) == 9999
        print(instances[1])