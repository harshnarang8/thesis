import itertools
import re
from typing import List

def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False

# this doesnt do anything, random code tests for sanity
def test1():
    with open('datasets/tot_tweets_train.txt', 'w') as wrfile:
        for is_divider, lines in itertools.groupby(myfile, _is_divider):
            # Ignore the divider chunks, so that `lines` corresponds to the words
            # of a single sentence.
            if not is_divider:
                # counter += 1 # tested
                fields = [line.strip().split() for line in lines]
                # print(fields)
                # first one is a double field with uid, sent
                # others are single fields
                for word in fields[1:]:
                    if word[0][0] == '@':
                        word.append("User")
                    else:
                        word.append(1)
                print(fields)

def test2():
    with open('tests/fixtures/semi_processed_semeval.txt', 'r') as read_file:
         for is_divider, lines in itertools.groupby(read_file, _is_divider):
            # Ignore the divider chunks, so that `lines` corresponds to the words
            # of a single sentence.
            if not is_divider:
                # counter += 1 # tested
                fields = [line.strip().split() for line in lines]
                # print(fields)
                for word in fields[1:]:
                    print(word)
                    # I wanna put in more preprocessing here
                    # this is because the tweets from tta have been extracted from scratch
                    # unlike the nice tweets set which was taken directly from semeval
                    # the sentiment though is taken from the semeval, since we have extracted tweets
                    # using the links from the original dataset which had labels for sentiment
                    
                print("")

def regex_processor(word: str):
    reg = r'[,\.\'\"\!]'
    answer = re.split(reg, word)
    return answer

def strempty(word: str):
    if word == '':
        return True
    return False

def test3():
    test_list = [['hum', 0], ['dono...jaisa', 1], ['hai,', 1], ['...kaun', 0], ['"yahan"!!', 0], ['.', 2]]
    replacement_list = []
    for idx, wordpair in enumerate(test_list):
        word, lang = wordpair
        processed_words = regex_processor(word)
        processed_words = [[x, lang] for x in processed_words if not strempty(x)]

        replacement_list.append((idx, processed_words))
    
    replacement_list = [ele for ele in reversed(replacement_list)]
    for idx, words in replacement_list:
        test_list = test_list[:idx] + words + test_list[idx+1:]
    
    print(test_list)

test3()