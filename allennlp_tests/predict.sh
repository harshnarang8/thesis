export OUTPUT_FILE=./predictions/predictions.json

allennlp predict \
  --output-file $OUTPUT_FILE \
  --predictor sent_predictor \
  --use-dataset-reader \
  --silent \
  ./modelv20/ \
  datasets/tta_input.raw#datasets/tta_sent.npy