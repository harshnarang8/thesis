from typing import Dict, Iterable, List

import logging
from overrides import overrides
import itertools
import re
import numpy as np
import torch

from allennlp.common.file_utils import cached_path
from allennlp.data import DatasetReader, Instance
from allennlp.data.fields import LabelField, TextField, SequenceLabelField, ArrayField
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from allennlp.data.tokenizers import Token, Tokenizer, WhitespaceTokenizer

logger = logging.getLogger(__name__)

def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False

@DatasetReader.register('laser-reader')
class LaserReader(DatasetReader):
    def __init__(self,
                 tokenizer: Tokenizer = None,
                 token_indexers: Dict[str, TokenIndexer] = None,
                 max_tokens: int = None,
                 **kwargs):
        super().__init__(**kwargs)
        self.tokenizer = tokenizer or WhitespaceTokenizer()
        self.token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}
        self.max_tokens = max_tokens

    def text_to_instance(self,
                         embeds: ArrayField,
                         sentiment: int = None) -> Instance:
        # text_tokens = self.tokenizer.tokenize(text)
        instance_fields: Dict[str, Field] = {"embeds": embeds}
        if sentiment is not None:
            instance_fields['label'] = LabelField(sentiment)
        return Instance(instance_fields)

    @overrides
    def _read(self, file_path: str) -> Iterable[Instance]:
        # if `file_path` is a URL, redirect to the cache
        # file_path = cached_path(file_path)
        # this is a special case, since we have raw embeddings and labels in separate files

        fp_embeddings, fp_labels = file_path.split('#')

        with open(fp_embeddings, "rb") as data_file:
            with open(fp_labels, "rb") as label_file:
                logger.info("Reading instances from lines in file at: %s", file_path)

                # here we simply read the entire numpy array
                embeddings = np.fromfile(data_file, dtype=np.float32, count=-1)
                dim = 1024 # this is fixed from the laser output
                embeddings.resize(embeddings.shape[0]//dim, dim)
                # this is because we extracted the labels in the dataset separately
                labels = np.load(fp_labels)
                for idx, el in enumerate(embeddings):
                    yield self.text_to_instance(ArrayField(el), labels[idx])