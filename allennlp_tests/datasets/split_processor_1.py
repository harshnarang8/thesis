# this file is for making a random assignment of language to words in the file
# the assignment for the moment is all hin or User if word begins with '@', not that it matters
# so that we can run it in combination with the nice tweets dataset
# that'll give us a place to start here
import itertools

# inspired from some random dataset reader file in allennlp
def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False


with open('split_tweets_ran1.txt', 'r') as myfile:
    # counter = 0 # Tested
    with open('split_tweets_ran1_1.txt', 'w') as wrfile:
        for is_divider, lines in itertools.groupby(myfile, _is_divider):
            # Ignore the divider chunks, so that `lines` corresponds to the words
            # of a single sentence.
            if not is_divider:
                # counter += 1 # tested
                fields = [line.strip().split() for line in lines]
                # print(fields)
                # first one is a double field with uid, sent
                # others are single fields
                for word in fields[1:]:
                    if word[0][0] == '@':
                        word.append("User")
                    else:
                        word.append(1)
                print(fields)
                
                for word, lang in fields:
                    wrfile.write(str(word) + "\t" + str(lang) + "\n")
                wrfile.write("\n")
        