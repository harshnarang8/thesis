import numpy as np

# x = np.load('tta_sent.npy')
# y = []
# for el in x:
#     y.append(str(el))

# z = np.array(y)
# with open('tta_sent_1.npy', 'wb') as f:
#     np.save(f, z)

import itertools

# inspired from some random dataset reader file in allennlp
def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False


with open('tsd_processed_new_tta.txt', 'r') as myfile:
    counter = 0 # Tested
    with open('new_tta_train.txt', 'w') as trainfile:
        with open('new_tta_val.txt', 'w') as valfile:
            with open('new_tta_test.txt', 'w') as tefile:
                for is_divider, lines in itertools.groupby(myfile, _is_divider):
                    # Ignore the divider chunks, so that `lines` corresponds to the words
                    # of a single sentence.
                    if not is_divider:
                        counter += 1 # tested
                        fields = [line.strip().split() for line in lines]
                        # print(fields)
                        wrfile = None
                        if counter < 900:
                            wrfile = trainfile
                        elif counter < 1100:
                            wrfile = valfile
                        else:
                            wrfile = tefile            
                        for word, lang in fields:
                            wrfile.write(str(word) + "\t" + str(lang) + "\n")
                        wrfile.write("\n")