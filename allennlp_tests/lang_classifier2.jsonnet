{
    "dataset_reader": {
        "type": "semeval-reader-2",
        "token_indexers": {
            "tokens": {
                "type": "single_id"
            },
            "tokens_2": {
                "type": "single_id"
            },
            "token_characters": {
                "type": "characters"
            }
        }
    },
    "train_data_path": "allennlp_tests/datasets/tot_tweets_train.txt",
    "validation_data_path": "allennlp_tests/datasets/nice_tweets_val.txt",
    "model": {
        "type": "basic_classifier",
        "text_field_embedder": {
            "type": "hinglish-embedder",
            "token_embedders": {
                "tokens": {
                    "type": "empty"
                },
                "tokens_2": {
                    "type": "empty"
                },
                "token_characters": {
                    "type": "character_encoding",
                    "embedding": {
                        "embedding_dim": 8,
                        "vocab_namespace": "token_characters"
                    },
                    "encoder": {
                        "type": "cnn",
                        "embedding_dim": 8,
                        "num_filters": 50,
                        "ngram_filter_sizes": [
                            5
                        ]
                    },

                    "dropout": 0.2
                }
            },
        },
        "dropout": 0.5,
        "seq2vec_encoder": {
            "type": "cnn",
            "num_filters": 40,
            "embedding_dim": 50,
            "output_dim": 80
        },
    },
    "data_loader": {
        "type": "default",
        "batch_size": 32
    },
    "trainer": {
        "optimizer": "adam",
        "num_epochs": 30,
        "patience": 10,
        "cuda_device": -1
    }
}