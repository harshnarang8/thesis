{
    "dataset_reader": {
        "type": "laser-reader",
    },
    "train_data_path": "allennlp_tests/datasets/train_split.raw#allennlp_tests/datasets/sentiments_train.npy",
    "validation_data_path": "allennlp_tests/datasets/val_split.raw#allennlp_tests/datasets/sentiments_val.npy",
    "model": {
        "type": "laser-hinglish-classifier",
        "dropout": 0.2,
        // "seq2vec_encoder": {
        //     "type": "cnn",
        //     "num_filters": 10,
        //     "embedding_dim": 65,
        //     "output_dim": 20
        // },
        "feedforward": {
            "input_dim": 1024,
            "num_layers": 4,
            "hidden_dims": [512, 256, 128, 64],
            "activations": "relu",
            "dropout": 0.5
        },
    },
    "data_loader": {
        "type": "default",
        "batch_size": 32
    },
    "trainer": {
        "optimizer": "adam",
        "num_epochs": 80,
        "patience": 20,
        "cuda_device": -1
    }
}