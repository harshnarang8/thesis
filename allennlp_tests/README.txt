# File information

# Notable thoughts
Let's collate this more as we go forward, I've created this file a few months
after starting work on the allennlp_tests folder.
Right now, I have excel files from the handmade dataset(HD).
Have to write a dataset reader class/Write a function to change the excel to
what I have for the previous runs format.
This should not be too hard, since I had the same styled raw format in semeval data (SD), but rextracted from twitter.

## Steps to follow:
Analyse code for v9/v10 dataset readers and how they work with the input format.
Next, think of how we'd approach coding the preprocessing function for the HD.
Next, write a function which takes input as HD file and outputs an SD-like file.

## Analysis
We need to see how we went from the full tweets to the line by line format.
Some functions corresponding to taking in the tweets and processing them should exist somewhere.
We know the script preprocessing occured before the dataset reader functioned.
The next step would be to see how the preprocessing function takes input, after locating it.
VarLSTMs.py seems to possess the script.

Upon further analysis, I observed that we have to just store our data as a [tuple(tweet, tuple(ID, sent))],
and save it in a pickle format.
This helps us basically run the entire thing in 1 go; since we'd just be using a different data file, but 
same format.
Let's write a module to do the same.
The code would be present in the twitterScraper folder.

Completed the tsd processing step, had to recheck the csnli code since it started giving import errors again like a few months ago.
tsd_processed_new_tta.txt has the entire data, and is copied to the allennlp_tests/datasets.
Now we just need to figure out how to use a model to get outputs, should be straightforward.

## Other objectives:
OBJ1: Get BERT up and running.
 
### OBJ1:

# Important data from runs
This run is a no learn evaluation for us. Let's observe what we get here.
The raw laser embeddings are in tta_input.raw.

Our dataset v1:
v1: 0.300
v2: 0.369
v3: 0.254
v4: 0.270
v5: 0.303
v6: 0.284
v7: 0.405
v8: 0.405
v9: 0.390
v10: NA
Various varieties of classifiers having different en/hi embeddings:
v11: 0.388 (without hi/en pre trained aligned vectors)
v12: 0.361
v13: 0.370
v14: 0.387
v15: 0.3667
v16: 0.398
v17: 0.415
v18: 0.444
v19: NA
Various varieties of laser classifiers, having different architectures of cnns:
v20: 0.339
v21: 0.340
v22: 0.311
v23: 0.310
v24: 0.329
v25: 0.313
v26: 0.313

Our dataset v2:
v1: 0.664, 0.46
v2: 0.985, 0.455
v3: 0.73, 0.51
v4: 0.86, 0.43
v5: 0.58, 0.48
v6: 0.759, 0.515
v7: 0.516, 0.445
v8: 0.516, 0.445
v9: 0.649, 0.445
v10: -
v11: 0.99, 0.43
v12: 0.99, 0.455
v13: 0.964, 0.46
v14: 0.98, 0.48
v15: 0.99, 0.45
v16: 0.99, 0.47
v17: 0.96, 0.47
v18: 0.97, 0.465
v19: -
v20: 0.56, 0.53
v21: 0.57, 0.52
v22: 0.52, 0.46
v23: 0.57, 0.53
v24: 0.49, 0.455
v25: 0.464, 0.44
v26: 0.471, 0.445