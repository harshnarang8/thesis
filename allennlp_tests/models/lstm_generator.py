# ok so we just come across the allennlp-models repository
# all our effort from the last 3 days gone to shit lulw
from typing import Dict, Union, List

import torch
from allennlp.data import Vocabulary
from allennlp.models import Model
from allennlp.modules import TextFieldEmbedder, Seq2VecEncoder
from allennlp.nn import util
from allennlp.training.metrics import CategoricalAccuracy

@Model.register('lstm-generator')
class LSTMGenerator(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 embedder: TextFieldEmbedder,
                 encoder: Seq2VecEncoder,
                 num_samples: int = None,
                 dropout: float = None):
        super().__init__(vocab)
        self.embedder = embedder
        # encoder is our super layer doing its thing
        self.encoder = encoder

        if dropout:
            self._dropout = torch.nn.Dropout(dropout)
        else:
            self._dropout = lambda x: x
        

        pass
    
    def _compute_loss(self,
                      lm_embeddings: torch.Tensor,
                      token_embeddings: torch.Tensor,
                      forward_targets: torch.Tensor) -> torch.Tensor:
        '''
        helper function to compute loss obviously
        input:
            lm_embeddings: shape - (batch_size, timesteps, dim)
        output:
            cross entropy, but how? this has to lead to perplexity calculations, figure out from
            thesamuel's code and complete this shit
            also, do we have to return only loss, or something else too?
        '''
        pass

    def forward(self,
                source: Dict[str, torch.LongTensor]) -> Dict[str, torch.Tensor]:
        '''
        Takes a dict, returns a dict
        Inspired from github.com/thesamuel/allennlp/blob/master/allennlp/models/language_model.py
        
        '''
        embedded_text = self.embedder(text)
        mask = util.get_text_field_mask(text)
        # Shape: (batch_size, encoding_dim)
        # note that encoding_dim is our gateway to predicting the next word
        encoded_text: Union[torch.Tensor, List[Torch.Tensor]] = self.encoder(embedded_text, mask)
        # basically what we wanna do is create a language model, and then for each output of the lstm
        # use a beam search to get top 5 words for both hindi and english, and forward it again
        # in any case, a beam search is for interesting models forward, so let's not focus on that
        # we can write a simple english model, and the only place we need to work on is our lossFunction
        # or prediction function  let's write a prediction function I guess
        token_ids = source.get('tokens')

        return_dict = {}

        # the basic idea for getting our perplexity measure is getting the sum of cross entropy
        # and exponentiating it
        # we just need to get a semblance of how we'd measure it during training

        if token_ids is not None:
            # this means we have original token ids
            # time to compute losses somehow
            pass
        else:
            # this means just return the predicted word, I think with a little more data
            # the following logic is not for this module, but for eventual understanding
            # since we're planning to use a beam search idea to generate hinglish sentences
            # we need uh, the contextual embedding, to do something, uhhh
            # oh yeah, we use the embedding to predict the top 5 words coming in next
            pass
        
        return return_dict