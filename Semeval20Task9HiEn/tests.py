import itertools

def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False

def count_tweets(myfile):
    with open(myfile, 'r') as read_file:
        counter = 0
        for is_divider, lines in itertools.groupby(read_file, _is_divider):
                # Ignore the divider chunks, so that `lines` corresponds to the words
                # of a single sentence.
                if not is_divider:
                    counter += 1
        print(counter)

count_tweets('tsd_processed_tta.txt')