from three_step_decoding import *
import pickle
import itertools
from tqdm import tqdm
# from allennlp.modules.token_embedders import Embedding
# from allennlp.data.vocabulary import Vocabulary
# from allennlp.common.file_utils import cached_path
# from ..allennlp_tests.dataset_readers import semeval_reader


tsd = ThreeStepDecoding('lid_models/hinglish', htrans='nmt_models/rom2hin.pt', etrans='nmt_models/eng2eng.pt')

temp_text_array = pickle.load(open('new_temp_text_array.pkl','rb'))
print(temp_text_array[0])

# for x in tsd.tag_sent(temp_text_array[0][0]):
#     print(x)

test_string = "Haan ye day kaafi good chal raha hai...Let's party raat mein."
# for x in tsd.tag_sent(test_string):
#     print(x)

# time to write comments
# so we can now produce proper words for tweets we have
# we now can save these words, and later write a script for going from tweet to tsd to predictor
# forget later for now, obviously, focus on the task at hand
# lets write a save function

def translate_and_save(tweets, write_file, log_file):
    # tweets is of the format Array(2-Tuple(String(tweet), 2-Tuple(Integer(ID), Integer(Sentiment))))
    f = open(write_file, 'w')
    l = open(log_file, 'w')
    translated_tweets = []
    missed_tweets = []
    flag = False
    for tweet in tqdm(tweets):
        temp_tweet = []
        temp_tweet.append(tweet[1])
        try:
            for word, trans, lang in tsd.tag_sent(tweet[0]):
                # word is the original word, trans is the transliterated form, lang is the detected lang
                temp_tweet.append((trans, lang))
            translated_tweets.append(temp_tweet)
        except:
            missed_tweets.append(tweet)
        # if flag:
        #     break
        # flag = True
    # print(translated_tweets)

    for tweet in translated_tweets:
        f.write(str(tweet[0][0]) + "\t" + str(tweet[0][1]) + '\n')
        for word, lang in tweet[1:]:
            if word[0] == '@':
                lang = "User"
            elif lang == "hi":
                lang = 1
            elif lang == "en" or lang == "ne":
                lang = 0
            else:
                # what should I put here instead?
                # I have weird shit  like named entities, but not always
                # the user tag takes care of @TwitterUsers
                # but not shit like Mathura, etc;
                # The easiest idea would be to just pass this word as English
                # But taking care of this exeption should vastly improve performance
                # currently the system just uses character embeddings and ignores language tags
                # So once we do some weird shit like processing users, universals, NEs
                # we should go up in performance
                # also note that these tags are language independent, which is a good thing
                lang = 2
            f.write(str(word) + "\t" + str(lang) + '\n')
        f.write("\n")
    for tweet in missed_tweets:
        l.write(str(tweet[1][0]) + '\t' + str(tweet[1][1]) + '\t' + tweet[0] + '\n')
    f.close()

translate_and_save(temp_text_array, "tsd_processed_new_tta.txt", "tsd_log_new_tta_0.txt")

def _is_divider(line: str) -> bool:
    empty_line = line.strip() == ""
    if empty_line:
        return True
    else:
        first_token = line.split()[0]
        if first_token == "-DOCSTART-":
            return True
        else:
            return False


def read_and_format(myfile, writefile, log_file):
    with open(myfile, "r") as data_file:
        with open(writefile, 'w') as wrfile:
            with open(log_file, 'w') as logfile:
                # counter = 0
                for is_divider, lines in itertools.groupby(data_file, _is_divider):
                    # Ignore the divider chunks, so that `lines` corresponds to the words
                    # of a single sentence.
                    if not is_divider:
                        # the counter is for testing purposes
                        # counter += 1
                        # if counter == 3:
                        #     break
                        fields = [line.strip().split() for line in lines]
                        # the code follows from the note described later
                        # lets just see what we have here
                        # print(fields)

                        # the following section is to process user IDs properly in the dataset
                        # useless semeval people, code is being reused from semeval_reader.py from the other folder

                        current_uid = None
                        current_user:str = None
                        bad_indexes = []
                        for idx, field in enumerate(fields):
                            # print(current_user)
                            if len(field) != 2:
                                bad_indexes.append(idx)
                                current_uid = None
                                current_user = None
                                # print(0)
                                continue
                            if field[1] == "User":
                                current_uid = idx
                                current_user = field[0]
                                # print(1)
                                continue
                            if current_user is not None:
                                # print(str("current_user: ") + current_user)
                                if field[0] == '_': # usernames having underscores have been separated sadly
                                    current_user = current_user + "_"
                                    bad_indexes.append(idx) #  removed the underscore
                                    # print(1.5)
                                    # print(current_user[-1])
                                    continue
                                elif current_user[-1] == "_":
                                    # print("YOLO")
                                    current_user = current_user + field[0]
                                    # print(current_user)
                                    bad_indexes.append(idx) # removed the current word
                                    if len(fields) > idx + 1:
                                        if fields[idx + 1][0] == '_':
                                            # basically our username has more underscores
                                            # print(2)
                                            continue
                                        else:
                                            fields[current_uid] = [current_user, "User"]
                                            current_uid = None
                                            current_user = None
                                            # print(3)
                                            continue
                                    else:
                                        fields[current_uid] = [current_user, "User"]
                                        current_uid = None
                                        current_user = None
                                        # print(4)
                                else:
                                    # print(current_user[-1])
                                    current_uid = None
                                    current_user = None
                        bad_indexes = [ele for ele in reversed(bad_indexes)]
                        for idx in bad_indexes:
                            fields.pop(idx)
                        # section over, fields now contain continuous user tags
                        # we're going to throw away the tags anyway, but since we're just joining the fields
                        # with a space, we needed to process the user tags first separately

                        # time to join the words to form original tweets
                        fields = [list(field) for field in zip(*fields)]
                        # print(fields)
                        tid = fields[0][0]
                        sent = fields[1][0]
                        text = " ".join(fields[0][1:])
                        
                        # here's the main printer hehe
                        wrfile.write(str(tid) + "\t" + str(sent) + "\n")
                        try:
                            for x in tsd.tag_sent(text):
                                word = x[1]
                                lang = x[2]
                                if word[0] == '@':
                                    lang = "User"
                                else:
                                    lang = convertor_tag(lang)
                                wrfile.write(word + "\t" + str(lang) + "\n")
                                # we print 
                            wrfile.write('\n')
                        except:
                            # the tsd.tag_sent fails for single word arguments
                            # so we simply write the text and print it, with an output to a log file
                            logfile.write(str(tid) + '\t' + str(sent) + '\t' + text + '\n')
                            wrfile.write(text + '\t' + '1' + '\n')
                            continue
                        # print(tsd.tag_sent(text))
                        # print('\n'.join(['\t'.join(x) for x in tsd.tag_sent(text)]))

                        # tuple if we need to save it, probably being commented out for now
                        # formed_tuple = ((tid, sent), text)
                        # print(formed_tuple)


def convertor_tag(lang):
    if lang == "en":
        return 0
    elif lang == "hi":
        return 1
    elif lang == "ne":
        return 0
    else:
        return 2


# read_and_format('nice_tweets.txt', 'tsd_processed_nice_tweets.txt', 'tsd_log_0.txt')
# NOTE we have to process nice tweets and TTA differently
# The translate_and_save function works fine for the TTA; it works on tweets extracted directly from twitter
# The nice tweets only need to be transliterated. But since we need to get a system up and running
# We might as well throw away the gold standard texts and work with the csnli generated tags 
# and since the csnli has an accuracy of 97%, it should work just fine for our case

# ugghhh, we also faced unprocessed user ID issues in the semeval dataset, we'll have to reuse
# that section of code from the semeval_reader.py code


# now the question is how do we get across this issue
# we can store the transliterated tweets as a txt file, and then use them further later
# or we can transliterate the tweets real time, while reading them as an instance in the model
# or we can transliterate the tweets as a part of the model

# the third option is the best here
# but for purposes of getting something out, I've chosen to go with the first option;

# and I'll maybe write a new reader which uses the csnli tsd routine to process the input
# when I create a final working submission

# I dont like anything right now what the fuck's going on; p.s. not in hte project; in life
# fucking sad shit's going on
# hope this file doesn't go in the final submission, atleast I should edit this out later


# another issue in preprocessing is that they've not properly divided the tweets into words,
# like hello...world is a single word, instead of 2 or 3, like 'hello...', 'world'; or
# 'hello', '...', 'world'. Haven't done anything for this yet, par lets see how it goes

# now we've run nice_tweets through tsd; we now have to run tta through tsd too

# now we gotta process hashtags, the transliteration API fucked up the hashtags
# basically if we encounter a #, we combine it with the next word.
# in other words it is the same as the users routine

def combine_hashtags(input_file, output_file):
    with open(input_file, 'r') as data_file:
        with open(output_file, 'w') as outfile:
            # counter = 0
            for is_divider, lines in itertools.groupby(data_file, _is_divider):
                # Ignore the divider chunks, so that `lines` corresponds to the words
                # of a single sentence.
                if not is_divider:
                    # the counter is for testing purposes
                    # counter += 1
                    # if counter == 3:
                    #     break
                    fields = [line.strip().split() for line in lines]
                    # the code follows from the note described later
                    # lets just see what we have here
                    # print(fields)

                    # the following section is to process user IDs properly in the dataset
                    # useless semeval people, code is being reused from semeval_reader.py from the other folder

                    current_uid = None
                    current_hash:str = None
                    bad_indexes = []
                    for idx, field in enumerate(fields):
                        # print(current_user)
                        if len(field) != 2:
                            bad_indexes.append(idx)
                            current_uid = None
                            current_hash = None
                            # print(0)
                            continue
                        if field[0] == "#":
                            current_uid = idx
                            current_hash = field[0]
                            # print(1)
                            continue
                        if current_hash is not None:
                            # print(str("current_hash: ") + current_hash)
                            if current_hash == "#":
                                # this means the hashtag is definitely incomplete
                                current_hash += field[0]
                                bad_indexes.append(idx)
                                continue

                            if field[0] == '_': # hashes having underscores have been separated sadly
                                current_hash = current_hash + "_"
                                bad_indexes.append(idx) #  removed the underscore
                                # print(1.5)
                                # print(current_user[-1])
                                continue
                            elif current_hash[-1] == "_":
                                # print("YOLO")
                                current_hash = current_hash + field[0]
                                # print(current_user)
                                bad_indexes.append(idx) # removed the current word
                                if len(fields) > idx + 1:
                                    if fields[idx + 1][0] == '_':
                                        # basically our username has more underscores
                                        # print(2)
                                        continue
                                    else:
                                        fields[current_uid] = [current_hash, "Hash"]
                                        current_uid = None
                                        current_hash = None
                                        # print(3)
                                        continue
                                else:
                                    fields[current_uid] = [current_hash, "Hash"]
                                    current_uid = None
                                    current_hash = None
                                    # print(4)
                            else:
                                # print(current_hash[-1])
                                fields[current_uid] = [current_hash, "Hash"]
                                current_uid = None
                                current_hash = None
                    bad_indexes = [ele for ele in reversed(bad_indexes)]
                    for idx in bad_indexes:
                        fields.pop(idx)
                    # section over, fields now contain continuous hashes

                    # print(fields)

                    # write fields to outfile
                    for word, lang in fields:
                        outfile.write(str(word) + '\t' + str(lang) + '\n')
                    outfile.write('\n')

# combine_hashtags('tsd_processed_nice_tweets.txt', 'tsd_processed_nt_1.txt')
