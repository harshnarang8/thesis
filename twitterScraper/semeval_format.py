import pandas as pd
import pickle
import numpy as np

df = pd.read_excel("data.xlsx", index_col=None)
print(df)
# create an id, and create and save tuples.
# verify what extra processing is needed for the same.
# the corresponding code is present
new_tta = []
tta_laser = []
tta_sent = []
for i in range(len(df)):
    new_tta.append((df.iloc[i, 1], (i, df.iloc[i, 2])))
    tta_laser.append(df.iloc[i, 1])
    tta_sent.append(str(df.iloc[i, 2]))

# write tta_laser to a text file

# write tta_sent to a npy array file
tta_np = np.array(tta_sent)
with open('tta_sent_train.npy', 'wb') as f:
    np.save(f, tta_np[0:900])
with open('tta_sent_val.npy', 'wb') as f:
    np.save(f, tta_np[900:1100])
with open('tta_sent_test.npy', 'wb') as f:
    np.save(f, tta_np[1100:])
print(tta_np)
print(tta_laser[:10])

# test_str = 'lets go\nliquid'

with open('new_tta_laser_train.txt', 'w') as f:
    with open('new_tta_laser_val.txt', 'w') as g:
        with open('new_tta_laser_test.txt', 'w') as h:
            x = None
            for i in range(len(df)):
                if i < 900:
                    x = f
                elif i < 1100:
                    x = g
                else:
                    x = h
                x.write(repr(tta_laser[i]))
                if (i != (len(df) - 1)):
                    x.write('\n')

# with open('new_temp_text_array.pkl','wb') as output:
#     pickle.dump(new_tta, output, pickle.HIGHEST_PROTOCOL)

